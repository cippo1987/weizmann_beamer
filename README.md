# Weizmann_Beamer

This is a beamer template for Weizmann Institute of science. 
This is derived from the work of  Lilyana Vaskova Vankova <lilqna.v@gmail.com>.
The header of the original Feather theme is preserved n the files

## Logo
As logo I used the Tree of Life, with small edits. To change the logo is necessary to modify the .sty files. 


## Color
The main color of the theme is defined at line 7 of the main tex:
`  \definecolor{Wblue}{RGB}{4,131,185}`
To change color I suggest [Paletton](https://paletton.com/)

## Packages
Amons the other I included `mhchem` available [here](https://ctan.org/pkg/mhchem?lang=en). This allow to write easily chemistry formula.
For example $`H_2O`$ can be written as `\ce{H2O}`

## Ratio
The slides come to the 16:9 ration format. Be careful to change it.
